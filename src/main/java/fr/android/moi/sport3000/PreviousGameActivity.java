package fr.android.moi.sport3000;

import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import java.util.Arrays;
import java.util.List;

import fr.android.moi.sport3000.model.Match;

public class PreviousGameActivity extends AppCompatActivity implements MatchFragment.OnFragmentInteractionListener {

    private List<Match> matches;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        matches = getMatchesFromDB();
        setContentView(R.layout.activity_previous_game);
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.linearLayout);

        if(matches != null){
            FragmentTransaction fgt =
                    getSupportFragmentManager().beginTransaction();

            int cpt = 1;
            for(Match match : matches){
                MatchFragment fragment = MatchFragment.newInstance();
                FrameLayout frameLayout = new FrameLayout(this);
                frameLayout.setId(cpt);
                frameLayout.setLayoutParams(new FrameLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.MATCH_PARENT));
                fgt.addToBackStack("new fragment");
                fgt.add(cpt, fragment);

                cpt++;
            }
            fgt.commit();

        }
    }

    private List<Match> getMatchesFromDB() {
        //TODO : get data from db
        return Arrays.asList(new Match(),new Match(), new Match());
    }

    public void onFragmentInteraction(Uri uri){
        //TODO : ?
        System.out.println("TEST");
    }
}
