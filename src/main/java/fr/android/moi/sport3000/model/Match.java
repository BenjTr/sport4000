package fr.android.moi.sport3000.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Match implements Serializable {
    private Location location;
    private Map<String, List<Round>> score;

    public Set<String> getPlayers(){
        return this.score.keySet();
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Map<String, List<Round>> getScore() {
        return score;
    }

    public void setScore(Map<String, List<Round>> score) {
        this.score = score;
    }
}
