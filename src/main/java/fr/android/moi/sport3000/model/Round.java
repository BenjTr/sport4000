package fr.android.moi.sport3000.model;

import java.io.Serializable;

public class Round implements Serializable {
    private Integer firstHit;
    private Integer secondHit;
    private Integer thirdHit;
    private Integer ballWeight;

    public Integer getFirstHit() {
        return firstHit;
    }

    public void setFirstHit(Integer firstHit) {
        this.firstHit = firstHit;
    }

    public Integer getSecondHit() {
        return secondHit;
    }

    public void setSecondHit(Integer secondHit) {
        this.secondHit = secondHit;
    }

    public Integer getThirdHit() {
        return thirdHit;
    }

    public void setThirdHit(Integer thirdHit) {
        this.thirdHit = thirdHit;
    }

    public Integer getBallWeight() {
        return ballWeight;
    }

    public void setBallWeight(Integer ballWeight) {
        this.ballWeight = ballWeight;
    }
}
