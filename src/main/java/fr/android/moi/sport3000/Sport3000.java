package fr.android.moi.sport3000;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class Sport3000 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lobby_layout);
    }

    public void myClickHandler(View view) {

        switch (view.getId()) {
            case R.id.newGame:
                Intent newGameIntent = new Intent(this, InitGameActivity.class);
                startActivity(newGameIntent);
                break;
            case R.id.previousGame:
                Intent previousGameIntent = new Intent(this, PreviousGameActivity.class);
                startActivity(previousGameIntent);
                break;
            default:
                break;
        }
    }
}
