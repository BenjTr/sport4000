package fr.android.moi.sport3000;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class InitGameActivity extends AppCompatActivity {

    private EditText player1;
    private EditText player2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init_game);

        player1 = (EditText) findViewById(R.id.player1);
        player2 = (EditText) findViewById(R.id.player2);
    }

    public void myClickHandler(View view) {
        switch (view.getId()) {
            case R.id.sumbit_user:
                Intent newGameIntent = new Intent(this, NewGameActivity.class);
                newGameIntent.putExtra("player_1", player1.getText().toString());
                newGameIntent.putExtra("player_2", player2.getText().toString());
                startActivity(newGameIntent);
                break;
            default:
                break;
        }
    }
}
