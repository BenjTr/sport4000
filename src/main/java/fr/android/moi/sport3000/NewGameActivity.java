package fr.android.moi.sport3000;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.android.moi.sport3000.model.Location;
import fr.android.moi.sport3000.model.Match;
import fr.android.moi.sport3000.model.Round;

public class NewGameActivity extends AppCompatActivity {

    private String player1;
    private String player2;

    private Boolean isPlayer1Turn = true;
    private Boolean firstShot = true;

    private Map<String, List<Round>> score;

    private Integer turn = 1;
    private static final int TAG_CODE_PERMISSION_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_game);
        Intent myIntent = getIntent();
        player1 = myIntent.getStringExtra("player_1");
        player2 = myIntent.getStringExtra("player_2");

        TextView player1TextView = (TextView) findViewById(R.id.player1Title);
        player1TextView.setText(player1);
        TextView player2TextView = (TextView) findViewById(R.id.player2Title);
        player2TextView.setText(player2);

        this.score = new HashMap<>();
        this.score.put(player1, new ArrayList<Round>());
        this.score.put(player2, new ArrayList<Round>());
        for(int i = 0 ; i < 10 ; i++){
            this.score.get(player1).add(new Round());
            this.score.get(player2).add(new Round());
        }

    }

    public String getPlayer1() {
        return player1;
    }

    public void setPlayer1(String player1) {
        this.player1 = player1;
    }

    public String getPlayer2() {
        return player2;
    }

    public void setPlayer2(String player2) {
        this.player2 = player2;
    }

    public void myClickHandler(View view) throws NoSuchFieldException, IllegalAccessException {

        switch (view.getId()) {
            case R.id.spare:
                if(spare()){
                    next();
                }
                break;
            case R.id.strike:
                if(strike()){
                    next();
                }
                break;
            default:
                Button button = (Button) findViewById(view.getId());
                if(addScore(button.getText().toString())){
                    next();
                }
                break;

        }

    }

    public Boolean strike() throws NoSuchFieldException, IllegalAccessException {
        TextView textViewScore;
        String textViewID;
        String key;

        if(this.isPlayer1Turn){
            key = this.player1;
            textViewID = "p1_"+ this.turn;
        }else{
            key = this.player2;
            textViewID = "p2_"+ this.turn;
        }

        Round r = this.score.get(key).get(turn - 1);
        if(r.getFirstHit() == null) {
            r.setFirstHit(10);
            r.setSecondHit(10);

            Field f = R.id.class.getDeclaredField(textViewID);
            textViewScore = (TextView) findViewById(f.getInt(f));
            textViewScore.setText("X");

            return true;
        }
        return false;
    }

    public Boolean spare() throws IllegalAccessException, NoSuchFieldException {
        TextView textViewScore;
        String textViewID;
        Round r;
        if(this.isPlayer1Turn){
            textViewID = "p1_"+ this.turn;
            r = this.score.get(this.player1).get(turn - 1);
        }else{
            textViewID = "p2_"+ this.turn;
            r = this.score.get(this.player2).get(turn - 1);
        }
        if(r.getFirstHit() != null){
            Field f = R.id.class.getDeclaredField(textViewID);
            r.setSecondHit(10);
            textViewScore = (TextView) findViewById(f.getInt(f));
            textViewScore.setText(textViewScore.getText()+" /");
            return true;
        }
        return false;

    }

    public Boolean addScore(String value) throws NoSuchFieldException, IllegalAccessException {
        TextView textViewScore;
        String textViewID;
        Round r = null;
        String key;
        if(this.isPlayer1Turn){
            key = this.player1;
            textViewID = "p1_"+ this.turn;
            r = this.score.get(this.player1).get(turn - 1);
        }else{
            key = this.player2;
            textViewID = "p2_"+ this.turn;
            r = this.score.get(this.player2).get(turn - 1);

        }

        Field f = R.id.class.getDeclaredField(textViewID);
        textViewScore = (TextView) findViewById(f.getInt(f));

        if(r.getFirstHit() == null){
            r.setFirstHit(Integer.parseInt(value));
            textViewScore.setText(value);
            return false;
        } else {
            if((r.getFirstHit()+Integer.parseInt(value)) >= 10){
                return false;
            }
            r.setSecondHit(Integer.parseInt(value));
            textViewScore.setText(textViewScore.getText()+" "+value);
            return true;
        }

    }

    public void next(){
        this.isPlayer1Turn = !this.isPlayer1Turn;
        if(this.isPlayer1Turn){
            this.turn++;
        }
        if(this.turn > 10){

           this.endGame();

            //TODO : add to BD

        }
    }

    public void endGame(){
        Match m = new Match();
        m.setScore(score);
        Location l = this.getLocation();
        if(l.getLatitude() != 0 && l.getLongitude() != 0){
            m.setLocation(l);

            Intent endGameIntent = new Intent(this, EndGameActivity.class);
            endGameIntent.putExtra("match", m);
            startActivity(endGameIntent);
        }

    }


    public Location getLocation(){
        Location l = new Location();
        l.setLongitude(0);
        l.setLatitude(0);

        LocationManager locationManager;
        String provider;

        // Get the location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the location provider -> use
        // default
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[] {
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION },
                    TAG_CODE_PERMISSION_LOCATION);

        }
        android.location.Location location = locationManager.getLastKnownLocation(provider);

        l.setLatitude((float) location.getLatitude());
        l.setLongitude((float) location.getLongitude());


        return l;

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("Req Code", "" + requestCode);
        if (requestCode == TAG_CODE_PERMISSION_LOCATION) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED ) {

                this.endGame();
            }
        }

    }


}
